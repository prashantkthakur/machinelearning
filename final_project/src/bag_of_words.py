import csv
import glob
import time
import json
import codecs
import nltk
import os
import re
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import nltk
from nltk.corpus import stopwords
import string

# Create a csv file


class BagOfWords:
    def __init__(self,):

    @staticmethod
    def yelp_reader(self,filename):
        with open(filename, 'r') as fp:
            for line in fp:
                yield json.loads(line)




    @staticmethod
    def process_yelp_data(business_file, review_file,search='Restaurants'):
        df_business = pd.read_csv(business_file)
        df_review = pd.read_csv(review_file)
        df_stars = df_review[['text','stars']]
        df_search = df_business[df_business['categories'].str.contains(search)]
        search_clean = df_search[['business_id', 'name']]
        df_combo = pd.merge(search_clean, df_stars, on='business_id')
        df_combo.to_csv("%s.csv"%search)


    # def searchTopic(self, filename):
    #     files = glob.glob(filename)
    #     df = pd.read_csv(files[0], encoding='utf-8')
    #     colmn = "text"
    #     df[colmn] = df[colmn].str.replace(r"http\S+", " ")  # Remove URL
    #     df[colmn] = df[colmn].str.replace(r"@\S+", " ")  # Remove '@words'
    #     # df[colmn] = df[colmn].str.replace(r"@", "") # Remove @ sign if any
    #     df[colmn] = df[colmn].str.lower()
    #     df[colmn] = df[colmn].str.replace(r"[^a-z0-9]", " ")
    #     dfHealthall = df[
    #         df[colmn].str.contains("hospital|doctor|immunization|digestion|vaccination|gym|medical|medicine")]
    #     # dfHealthall = df[df[colmn].str.contains("infection|inflammation|liver|atrophy")]
    #     # dfFood = df[df[colmn].str.match("food|restaurant|recipe|bacon|beef|pickle|mushroom|grits|egg|millet\
    #     #                                  |soysauce|taco|fruits|vegetable")]
    #     # dfHealthpolitics = dfHealthall[dfHealthall['text'].str.contains("trump")]
    #     # dfPolitics = df[df[colmn].str.contains("politics|election|government|govern|campagin|modi|trump|abe\
    #     #                                        |govt|legislation|constitution")]
    #     # dfBusiness = df[df[colmn].str.contains("business|market|commerce|factory|company|companies|factories|product")]
    #     # dfCrime = df[df[colmn].str.contains("cops|police|illegal|crime|prison|violence|murder|rape|dead|kill")]
    #     # dfCrimefilter = dfCrime[dfCrime[colmn].str.contains("linux|skill")]
    #     # dfPolitics.to_csv("Politics.csv",index=False)
    #     # dfBusiness.to_csv("Business.csv",index=False)
    #     # dfWrite = pd.concat([dfCrime, dfCrimefilter]).drop_duplicates(keep=False)  # Remove duplicate drops df
    #     # dfHealth.to_csv("Health-no-trump.csv",index=False)
    #     # dfFood.to_csv("Food.csv", index=False)
    #     # dfWrite.to_csv("Crime.csv", index=False)
    #     dfHealthall.to_csv("./tweets/Health.csv", index=False)
    #     print(dfHealthall['text'])

    def preProcess(self, filepath, filename, separator=",", action='write'):
        # Use iterator if there are many files.
        fileIter = glob.iglob("./{}/{}".format(filepath, filename))
        # files = glob.glob("100Crime.csv")
        # df = pd.read_csv('sample.csv', encoding='utf-8')
        for fn in fileIter:
            print("Processing {}".format(fn))
            df = pd.read_csv(fn, encoding='utf-8', sep=separator, usecols=['id_str', "text"])
            # df = pd.read_csv(fn, encoding='utf-8', sep=separator)
            # Add columns if there is no header in csv file.
            # df.columns = ['id_str','text']
            scolmn = 'text'
            dcolmn = 'text'
            dfc = self.replaceTexts(df, scolmn, dcolmn)
            dft = self.tokenizeLemmatize(dfc, dcolmn)  # Add tokens column to the dataframe.
            dft["tokens"] = df["tokens"].str.join(" ")  # Store tokens as sentence rather than list.
            dfw = dft[df["tokens"].str.split().str.len() > 3]
            dfw.drop_duplicates(["text"])
            if action == 'write':
                dfw.to_csv('./{}/pre_{}'.format(filepath, os.path.basename(fn).split(".")[0] + ".csv"), index=False)
            else:
                return dfw

    # def loadSample(self, filepath, filename):
    #     file = glob.glob("./{}/{}".format(filepath, filename))
    #     df = pd.read_csv(file)
    #     return df
    #
    # def addSample(self, allTokens):
    #     filepath = './files'
    #     # filename='100*.csv'
    #     # dfm = self.preProcess(filepath, filename, action='return')
    #     dfs = self.loadSample(filepath, "zzsample.csv")
    #     dfs["token"] = dfs["tokens"].apply(lambda x: [y for y in x if y in allTokens])
    #     dfs["token"] = dfs[dfs["token"].str.split.str.len() > 2]
    #     dfs.to_csv("./{}/processed_sampled.csv".format(filepath))
    @staticmethod
    def text_process(text):
        """
        Takes in a string of text, then performs the following:
        1. Remove all punctuation
        2. Remove all stopwords
        3. Returns a list of the cleaned text
        """
        # Check characters to see if they are in punctuation
        nopunc = [char for char in text if char not in string.punctuation]

        # Join the characters again to form the string.
        nopunc = ''.join(nopunc)

        # Now just remove any stopwords
        return [word for word in nopunc.split() if word.lower() not in stopwords.words('english')]


    def clean_review(self,df):
        df['text'] = df['text'].apply(lambda x: self.decontracted(x))
        df['text'] = df['text'].apply(lambda x: x.lower())
        df['text'] = df['text'].apply(lambda x: x.replace('\n', ' '))
        df['text'] = df['text'].apply(lambda x: x.replace('\t', ' '))
        # df['text'] = df['text'].str.replace(r"@\S+", '')
        df['text'] = df['text'].str.replace(r"http\S+", "")
        df['token'] = df['text'].apply(
            lambda x: ''.join([y for y in x.split() if y not in stopwords.words('english')]))
        # df['text'].apply(lambda x: [y for y in x if y not in stopwords.words('english')])

    @staticmethod
    def decontracted(phrase):
        # specific
        phrase = re.sub(r"won't", "will not", phrase)
        phrase = re.sub(r"can\'t", "can not", phrase)
        phrase = re.sub(r"w/", "with", phrase)
        phrase = re.sub(r"n\'t", " not", phrase)
        phrase = re.sub(r"\'re", " are", phrase)
        phrase = re.sub(r"\'s", " is", phrase)
        phrase = re.sub(r"\'d", " would", phrase)
        phrase = re.sub(r"\'ll", " will", phrase)
        phrase = re.sub(r"\'t", " not", phrase)
        phrase = re.sub(r"\'ve", " have", phrase)
        phrase = re.sub(r"\'m", " am", phrase)
        return phrase

    @staticmethod
    def replaceTexts(df, scolmn, dcolmn):
        df[dcolmn] = df[scolmn].str.replace(r"http\S+", "")  # Remove URL
        df[dcolmn] = df[scolmn].str.replace(r"@\S+", "")  # Remove @words
        # df[dcolmn] = df[scolmn].str.replace(r"@", "") # Remove @ sign if any
        df[dcolmn] = df[scolmn].str.lower()
        df[dcolmn] = df[scolmn].str.replace(r"[^a-z0-9]", " ")
        # df[dcolmn] = df[scolmn].apply(lambda x: ' '.join(filter(lambda y: not y.isdigit(), x.split()))) # Remove numbers
        df[dcolmn] = df[scolmn].str.findall('\w{3,}').str.join(' ')  # Remove word less than 3 char.
        # df[colmn] = df[colmn].apply(lambda x: ' '.join(filter(lambda y: len(y) > 3, x.split())))
        return df

    def sanitizeChar(self, ifile, output):
        fp = codecs.open(ifile, 'r', encoding='utf-8', errors='replace')
        for line in fp:
            output.write(line)

    @staticmethod
    def tokenizeLemmatize(df, colmn):
        # tokenizer = nltk.tokenize.RegexpTokenizer(r'\w+')
        tokenizer = nltk.TweetTokenizer(strip_handles=True, reduce_len=True)
        stop_words = set(nltk.corpus.stopwords.words("english"))
        stemmer = nltk.stem.SnowballStemmer("english")
        lemmatizer = nltk.stem.WordNetLemmatizer()
        df["tokens"] = df[colmn].fillna("").apply(tokenizer.tokenize)
        df["tokens"] = df["tokens"].apply(lambda x: [y for y in x if y not in stop_words])  # Remove stop words.
        df["tokens"] = df["tokens"].apply(lambda x: [lemmatizer.lemmatize(y) for y in x])
        df["tokens"] = df["tokens"].apply(lambda x: [stemmer.stem(y) for y in x])  # Stem the tokens
        return df


def main():
    print("Executing...")
    # BagOfWords.sanitize(BagOfWords)
    filepath = "../fetch_tweets/"
    filename = 'other_tweets.csv'
    # filename = "goodhealth.txt"
    BagOfWords.preProcess(BagOfWords, filepath, filename)
    # BagOfWords.searchTopic(BagOfWords)


if __name__ == '__main__':
    main()
